package com.mantech.dev2.demoproject.controlloer;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class PeopleController {

    @GetMapping("/fruits")
    public List<String> getFruits() {
        return Arrays.asList("apple", "orange", "banana");
    }


}
