package com.mantech.dev2.demoproject.service;

import com.mantech.dev2.demoproject.config.TaskChannel;
import com.mantech.dev2.demoproject.model.TaskCommand;
import com.mantech.dev2.demoproject.model.TaskEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class ActionService {
    private final TaskChannel taskChannel;
    Logger logger = LoggerFactory.getLogger(ActionService.class);

    @Autowired
    public ActionService(TaskChannel taskChannel) {
        this.taskChannel = taskChannel;
    }

    @StreamListener(
            target = TaskChannel.INPUT)
    public void onTaskCommand(TaskCommand taskCommand) {
        logger.info("onTaskCommand target:{}, action:{}", taskCommand.getTarget(), taskCommand.getAction());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            //just ignore...
        }
        TaskEvent taskEvent = new TaskEvent();
        taskEvent.setName(taskCommand.getTarget());
        taskEvent.setType(taskCommand.getAction());
        if("eat".equals(taskCommand.getAction())) {
            taskEvent.setDescription("it's yammy!");
        }
        Message<TaskEvent> msg = MessageBuilder.withPayload(taskEvent).build();
        taskChannel.taskOutput().send(msg);
    }

}
