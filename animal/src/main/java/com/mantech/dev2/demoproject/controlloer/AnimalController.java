package com.mantech.dev2.demoproject.controlloer;

import com.mantech.dev2.demoproject.config.TaskChannel;
import com.mantech.dev2.demoproject.model.TaskCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class AnimalController {

    private final TaskChannel taskChannel;

    @Autowired
    public AnimalController(TaskChannel taskChannel) {
        this.taskChannel = taskChannel;
    }

    @GetMapping("/animals")
    public List<String> getFruits() {
        return Arrays.asList("dog", "orange", "cat");
    }

    @GetMapping("/eat/{fruit}")
    public boolean eatFruit(@PathVariable String fruit) {
        TaskCommand taskCommand = new TaskCommand();
        taskCommand.setAction("eat");
        taskCommand.setTarget(fruit);
        Message<TaskCommand> msg = MessageBuilder.withPayload(taskCommand).build();
        taskChannel.taskOutput().send(msg);
        return true;
    }

}
