package com.mantech.dev2.demoproject.config;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface TaskChannel {
    String INPUT = "task-action-event";

    @Input(TaskChannel.INPUT)
    SubscribableChannel taskInput();

    @Output("task-action-command")
    MessageChannel taskOutput();
}
